(function($) {
  $(function() {
    $('.comment .submitted, .comment .links').css({
      opacity: 0.2
    });
    $('.comment').hover(function() {
      $('.submitted, .links', this).stop().animate({
        opacity: 1
      }, 500);
    }, function() {
      $('.submitted, .links', this).stop().animate({
        opacity: 0.2
      }, 500);
    });
  });
})(jQuery);